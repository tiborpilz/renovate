module.exports = {
  endpoint: 'https://gitlab.com/api/v4/',
  platform: 'gitlab',
  persistRepoData: true,
  logLevel: 'debug',
  autodiscover: false,
  autodiscoverFilter: 'tiborpilz/*',
  autodiscoverNamespaces: ['tiborpilz'],
  repositories: [
    'tiborpilz/infrastructure',
    'tiborpilz/nixOS'
  ],
};
